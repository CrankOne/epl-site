Name: Sergey Mamon
Date of birth: 1989
Position in the EPL: laboratory assistant researcher
Position in the TPU: laboratory assistant researcher
About:

Education
2014-present Ph. D, Theoretical Physics, National research Tomsk State University, Russia.
2007-2013 Nuclear Physics and Technologies, National Research Tomsk Polytechnic University,
Russia, Engineer-physicist.

Reseach Collaborations
2014-present SND, Budker Ins., Novosibirsk, Russia.
2016-present COMPASS, CERN, Jeneva, Switzerland.

Field of Scientific Interests
Experimental High Energy Physics, Data Analysis.

Professional quality
Sociability,, energy,, focus on results.

Extra information
OS UNIX, Debian GNU/Linux, Windows NT

Languages English (technical)
Russian (native)

