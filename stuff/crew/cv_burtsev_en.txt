Personal data
Name: Vitalii Burtsev
Date of birth: 1991
Position in the EPL: laboratory assistant researcher
Position in the TPU: aspirant, Senior Lecturer

About:

Education

07/1997 - 07/2008
Secondary school
Russia, Muravlenko

from 07/2008
Tomsk state university of control systems and radioelectronics
Department of Industrial Electronics

Professional background

from 01.06.2010
Scientific Research Institute of Automation and Electromechanics
from 01.05.2016
Mathematical physics department
Tomsk Polytechnic University

Languages
Native: russian
Foreign: english B2 (certificated in Tomsk Polytechnic University, 2016)

Contacts
Personal phone: +7-952-152-4686
E-mail:  Azzzrail@bk.ru
Skype: azzzraelo
