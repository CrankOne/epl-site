Name: Prof. Valery Lyubovitsky
Date of birth: 1963
Position in the EPL: Group leader
Position in the TPU: Professor
About:
    Education
    09/1970 - 06/1980
    Secondary School
    Kostroma, Russia

    09/1980 - 06/1985 University Education
    Physical Faculty
    Tomsk State University, Russia

    06/1986 - 06/1989 PhD Study
    Theoretical and Mathematical Physics
    Tomsk State University, Russia
    and Joint Institute for
    Nuclear Research (JINR),
    Dubna, Russia
    Defend: Tomsk State University, 18.05.1989

    09/1996 - 06/1998 Second Doctoral Degree Study
    Theoretical Physics
    Tomsk State University, Russia
    and Joint Institute for
    Nuclear Research (JINR),
    Dubna, Russia
    Defend: JINR, Dubna, 24.06.1998

    Complete Professional Background
    from 10/1999 - Leading Scientific Researcher
    Institute of Theoretical Physics
    Tübingen University, Germany
    from 06/1998 - Professor
    Department of Quantum Field Theory
    Tomsk State University
    from 02/1990 - Leading Scientific Researcher
    Department of Quantum Field Theory
    Tomsk State University
    from 12/2014 - Professor
    Mathematical Physics Department
    Tomsk Polytechnic University
    06/1989 - 06/2000 - Leading Scientific Researcher
    Joint Institute for
    Nuclear Research (JINR),
    Dubna, Russia

    Honours and Awards
    1995 First Prize - Tomsk Region Administration in Education and Science
    1998 Second Prize - JINR (Dubna, Russia) in Theoretical Physics
    2016 - Journal of Physics G: Nuclear and Particle Physics
    Outstanding Reviewer

    Highlights of Significant Contributions
    1. Developed methods of nonlocal quantum field theory for the evaluation of matrix
    elements involving bound states of quarks and gluons — hadrons and exotic states.
    2. Developed a manifestly Lorentz-covariant and gauge invariant field-theoretical ap-
    proach for the study of light, single, double and triple heavy baryons.
    3. Developed QCD+QED framework for the study of hadronic atoms.
    4. Developed a quantum field approach for the study of hadronic molecules, tetraquarks
    and hybrids.
    5. Developed a AdS/QCD model for the study of mesons, baryons and exotic states.

    Research Collaborations
    1. Germany:
    Profs. W. Vogelsang, T. Gutsche, A. Faessler (University of Tübingen), Prof. J.G.
    Körner (University of Mainz), Prof. P. Kroll (University of Wuppertal), Dr. A. Ruset-
    sky (University of Bonn)
    2. Chile:
    Profs. I. Schmidt, S. Kovalenko, C. Dib, S. Kuleshov, Drs. A. Vega, O. Castillo-
    Felisola, C. Corral (Universidad Tecnica Federico Santa Maria, Valparaiso), Prof. J.C.
    Helo (La Serena University)
    3. USA:
    Prof. S.J. Brodsky (SLAC), Prof. B.R. Holstein (University of Massachusetts), Prof.
    R.F. Lebed (Arizona State University), Prof. E. Chudakov, Dr. A. Somov and GlueX
    Coll. (JLab, Newport News)
    4. Switzerland:
    Prof. J. Gasser (University of Bern), Prof. M.P. Locher (PSI)
    5. Russia:
    Prof. M.A. Ivanov, G.V. Efimov, Dr. Yu.S. Surovcev (JINR, Dubna), Prof. V.G.
    Neudatchin, Drs. I.T. Obukhovsky, D.I. Fedorov, L.L. Sviridova (Moscow State Uni-
    versity), Profs. E.G. Drukarev, M.G. Ryskin, Dr. V.A. Sadovnikova (Petersburg
    Nuclear Physics Institute), Dr. A. Zhevlakov (Tomsk State University)
    6. Spain:
    Prof. M.J. Vicente Vacas, Dr. A.N. Hiller Blin (University of Valencia)
    7. Japan:
    Prof. S. Kumano (KEK, Japan)
    8. France:
    Prof. R. Vihn Mau (University of Paris VI-VII)4
    9. China:
    Profs. Y.B. Dong, P. Wang, P.N. Shen, (IHEP, Beijing),
    Prof. Y.L. Ma (Jilin Univ.)
    10. Thailand:
    Dr. K. Pumsa-ard (Bangkok University), Prof. Y. Yan, Drs. K. Khosonthongkee, S.
    Cheedket (Suranaree Univ. of Technology)

    Work in experimental collaborations
    1. GlueX at JLab (USA): search for gluon content in hadrons
    2. NA58 (COMPASS), NA64 and SHiP at SPS (CERN): hadron structure, search for
    light dark matter and new hidden particles, penetrating particles, lepton flavor viola-
    tion, decays of τ -leptons, neutrino oscillations, baryon asymmetry in the Universe and
    inflation.
    3. CMD-3 (Budker INP, Novosibirsk): study of electron-positron annihilation into hadrons

    Teaching experience
    1. Classic and Quantum Mechanics
    2. Quantum Scattrting Theory
    3. Mathematical Physics
    4. Electrodynamics
    5. Particle Physics
    6. Quantum Field Theory

    Supervision of PhD Students and Postdocs
    1. Nadja Ladygina (JINR, Dubna, Russia) 1992 - 1995
    2. Igor Anikin (JINR, Dubna, Russia) 1993 - 1996
    3. Sampart Cheedket (Tübingen University) 1999 - 2002
    4. Khosonthongkee Kanchai (Tübingen University) 2000 - 2003
    5. Kem Pumsa-ard (Tübingen University) 2001 - 2006
    6. Dr. Ping Wang (Tübingen University) 2001 - 2003
    7. Francesco Giacosa (Tübingen University) 2002 - 2005
    8. Jan Kuckei (Tübingen University) 2002 - 2006
    9. Diana Nikmorus (Tübingen University) 2003 - 2006
    10. Dr. Takashi Inoue (Tübingen University) 2002 - 2004
    11. Dr. Yong-Liang Ma (Tübingen University) 2006 - 2008
    12. Chalump Oonariya (Tübingen University) 2006 - 2008
    13. Tanja Branz (Tübingen University) 2007 - 2011
    14. Paulos Chatzis (Tübingen University) 2008 - 2012
    15. Sergey Mamon (Tomsk State University) from 2014
    16. Renat Dusaev (Tomsk Polytechnic University) from 2015
    17. Bogdan Vasilishin (Tomsk Polytechnic University) from 2015
    18. Alexandr Chumakov (Tomsk Polytechnic University) from 2016
    19. Vitalii Burtsev (Tomsk Polytechnic University) from 2016
    20. Ivan Kuznetsov (Tomsk Polytechnic University) from 2016
    21. Dr. Viacheslav Gauzshtein (Tomsk Polytechnic University) from 2016

    Referee
    1. Phys. Rev. C, D; Phys. Rev. Lett.
    2. Nucl. Phys. A, B; Phys. Lett. B ab 2001
    3. JETP, JETP Lett., Physics of Atomic Nuclei ab 1996
    4. JHEP, Z. Phys., Ann. der Phys., Eur. Phys. J. A, C
    5. Int. J. Mod Phys. A, C

    \center {\bf List of most important publications}


    \begin{enumerate}
    
    \item
    {\bf ``QCD compositeness as revealed in exclusive vector boson reactions        
    through double-photon annihilation:                                             
    $e^+ e^- \to \gamma \gamma^\ast \to \gamma V^0 $ and                            
    $e^+ e^- \to \gamma^\ast \gamma^\ast \to V^0 V^0$''}
      \\{}S.~J.~Brodsky, R.~F.~Lebed and V.~E.~Lyubovitskij.
      \\{}Phys.\ Lett.\ B {\bf 764}, 174 (2017)
    
    \item
    {\bf ``Nucleon parton distributions in a light-front quark model''}
      \\{}T.~Gutsche, V.~E.~Lyubovitskij and I.~Schmidt.
      \\{}Eur.\ Phys.\ J.\ C {\bf 77}, 86 (2017)
    
    \item
    {\bf ``Deuteron electromagnetic structure functions and                         
    polarization properties in soft-wall AdS/QCD''}
    \\{} T.~Gutsche, V.~E.~Lyubovitskij and I.~Schmidt,
    \\{}Phys.\ Rev.\ D {\bf 94}, 116006 (2016)
    
    
    \item
    {\bf ``Rare baryon decays $\Lambda_b \to \Lambda \ell^+ \ell^-$                 
    $(\ell=e, \mu, \tau)$                                                           
    and $\Lambda_b \to \Lambda \gamma$ : differential and total rates,              
    lepton- and hadron-side forward-backward asymmetries''}
    \\{}T.~Gutsche, M.~A.~Ivanov, J.~G.~Korner, V.~E.~Lyubovitskij,
    P.~Santorelli
    \\{}Phys.\ Rev.\ D {\bf 87}, 074031 (2013)
    
    \item
    {\bf ``Dilaton in a soft-wall holographic approach to mesons and baryons''}
    \\{}T.~Gutsche, V.~E.~Lyubovitskij, I.~Schmidt, A.~Vega
    \\{}Phys.\ Rev.\ D {\bf 85}, 076003 (2012)
    
    \item
    {\bf ``Hadronic molecule structure of the $Y(3940)$ and $Y(4140)$''}
    \\{}T.~Branz, T.~Gutsche, V.~E.~Lyubovitskij
    \\{}Phys.\ Rev.\ D {\bf 80}, 054019 (2009)
    
    \item
    {\bf ``Hadronic atoms in QCD + QED''}
      \\{}J.~Gasser, V.~E.~Lyubovitskij, A.~Rusetsky
      \\{}Phys.\ Rept.\  {\bf 456}, 167 (2008)
    
    
    \item
    {\bf ``Strong and radiative decays of the $D_{s0}^\ast(2317)$ meson             
    in the $DK$-molecule picture''}
    \\{}A.~Faessler, T.~Gutsche, V.~E.~Lyubovitskij, Y.~L.~Ma
    \\{}Phys.\ Rev.\ D {\bf 76}, 014005 (2007)
    
    
    \item
    {\bf ``Scalar nonet quarkonia and the scalar glueball:                          
    Mixing and decays in an effective chiral approach''}
    \\{}F.~Giacosa, T.~Gutsche, V.~E.~Lyubovitskij, A.~Faessler
    \\{}Phys.\ Rev.\ D {\bf 72}, 094006 (2005)
    
    \item
    {\bf ``Heavy baryon transitions in a relativistic three-quark model''}
      \\{}M.~A.~Ivanov, V.~E.~Lyubovitskij, J.~G.~Korner, P.~Kroll
      \\{}Phys.\ Rev.\  D {\bf 56}, 348 (1997)
    
    \item
    {\bf ``Electromagnetic form-factors of nucleons in \\                           
    a relativistic three quark model''}
    \\{}M.~A.~Ivanov, M.~P.~Locher, V.~E.~Lyubovitskij
    \\{}Few Body Syst.\  {\bf 21}, 131 (1996)
    
    \end{enumerate}

    Publication record
    1. Reviews — 5
    Phys. Rept., Ann. Rev. Nucl. Part. Sci., Phys. Part. Nucl.,
    Prog. Part. Nucl. Phys., Rep. Prog. Phys.
    2. Referred journals > 200
    Phys. Rev. (104), Phys. Rev. Lett. (1), Phys. Lett. B (16), Z. Phys. (6), Phys.
    Atom. Nucl. (6),
    J. Phys. (10), Int. J. Mod. Phys. (6), Eur. Phys. J. (4),
    JETP Lett. (2), Few Body Syst. (9), etc.
    3. Conference proceedings > 70
