#!/bin/sh

# TODO: check, if venv is set-up. If not, run:
#  $ import xml.etree.ElementTree as ET
#  $ pip install flask
#  $ pip install Flask-Babel
# OR:
#  $ pip install -r requirements.txt
# (when requirements.txt will appear)

if [ ! -d .venv/bin ] ; then
    echo "No .venv/bin dir found. Setting up the virtualenv..."
    virtualenv --prompt "venv-3\n" -p /usr/bin/python3 .venv
    . ./.venv/bin/activate
    pip install -r requirements.txt
    deactivate
fi

. ./.venv/bin/activate
export FLASK_APP=eplweb/__init__.py
export FLASK_DEBUG=1
flask run --host=0.0.0.0
