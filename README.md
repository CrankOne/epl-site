# Elementary Particles Lab site

This tiny project written on py-Flask is devoted to our Elementary
Particles Laboratory.

To run, type:

    $ FLASK_APP=eplweb/__init__.py flask run
