import re
import xml.etree.ElementTree as ET

"""
To be brief in python code inside Jinja templates we're using the getting
attributes method of some object typically referred as "oC" (object Content).

The "attributes" of this object refers to XML nodes containing text or other
nodes. There few peculiarities connected with querying such objects:
    * The getattr uses the so-called "CamelCase" notation while XML nodes we
    had named in "spinal-case". We use a conversion function to provide
    interoperability between them.
    * If multiple nodes may be returned by this getter, we have to choose one
    with appropriate locale (setted as XML) attribute.

The user code may desire to retrieve the XML content by the X-Path. For this
specific case we provide the X() method accepting the X-Path string (the
invokation will be directly forwarded to the
xml.etree.ElementTree.Element.findall() call.
"""

rxFirstCap = re.compile('(.)([A-Z][a-z]+)')
rxAllCapRe = re.compile('([a-z0-9])([A-Z])')


class NoSuchElementError(KeyError):
    """
    Exception indicating that requested XML node(s) aren't in the current XML
    document.
    """
    def __init__(self, message, key, *args, altKey=None, **kwargs):
        self.key = key
        self.altKey = altKey
        super(NoSuchElementError, self).__init__(message, *args, **kwargs)


def convert( pt ):
    """
    Performs conversion between CamelCase and spinal-case for given string.
    """
    s1 = rxFirstCap.sub( r'\1-\2', pt )
    return rxAllCapRe.sub( r'\1-\2', s1 ).lower()

class ContentObject(object):
    """
    The class for objects representing particular XML node. The class
    provides some easy-peasy syntactic handles for retrieving the node's
    content considering the naming differencies and locale support.
    """
    def __init__(self, node):
        self.node = node

    def _objectify_nodes(self, nodes):
        if 0 == len(nodes):
            raise AssertionError( 'Zero-length nodes list given.' )
        return ContentsList(nodes)

    def _get_available_childs_str(self):
        #lst = list(self.node)
        return ', '.join( list( '"%s"'%n.tag for n in self.node ) ) or '(none)'

    def __getattr__(self, key):
        return self.retrieve_node( key, xml=False )

    def attr(self, key):
        if type(key) is not str:
            raise AssertionError( "Content lookup error: non-string index"
                " given as an attribute name (\"%s\")."%key )
        return self.node.get(key)

    def retrieve_node(self, key, xml=False ):
        """
        At first, tries to retrieve element(s) with given key directly, with
        xml.etree.ElementTree.Element.findall() method. If an empty list will
        be returned and key contains the non-first capital letter(s), tries to
        findall() the sub-element(s) again. If an empty list returned, raises
        NoSuchElementError.
        """
        fR = self.node.findall(key)
        if fR:
            # TODO: conversion/locale
            return self._objectify_nodes(fR)
        if not rxAllCapRe.search( key ):
            raise AttributeError
            #availableChilds = self._get_available_childs_str()
            #raise NoSuchElementError( "Content lookup error: has no"
            #                          " sub-element \"%s\". Available"
            #                          " childs: %s."%(key, availableChilds) \
            #                         , key )
        key2 = convert(key)
        fR = self.node.findall(key2)
        if fR:
            # TODO: conversion/locale
            return self._objectify_nodes(fR)
        availableChilds = self._get_available_childs_str()
        raise AttributeError
        #raise NoSuchElementError( "Content lookup error: has no sub-element \"%s\""
        #                          " or \"%s\". Available childs: %s."%( \
        #                                  key, key2, availableChilds ) \
        #                         , key \
        #                         , altKey=key2 )

    def __str__(self):
        return self.node.text

    #def __html__(self):
    #    return self.node.find('literalHTML')[0].text

class ContentsList(object):
    def __init__(self, nodes):
        self.nodes = list(map( ContentObject, nodes ))

    def as_singular(self):
        if 1 == len( self.nodes ):
            return self.nodes[0]
        #raise NotImplementedError('Locale selection')
        loc = "en"  # TODO: get current user's locale here
        locEls = list(filter( lambda el: el.node.get('lang', 'en') == 'en', self.nodes ))
        if len(locEls) > 1:
            raise LookupError( 'Few elements (%d: %s) are available while only one'
                               ' is expected at node.'%(len(locEls) \
                             , ', '.join(n.node.tag for n in self.nodes) ) )
        elif 0 == len(locEls):
            raise AssertionError( 'Empty elements list met.' )
        return locEls[0]

    def __iter__(self):
        return (n for n in self.nodes)

    def __len__(self):
        return len(self.nodes)

    def __str__(self):
        return self.as_singular().__str__()

    def __getattr__(self, key):
        return self.as_singular().retrieve_node( key, xml=False )

    def attr(self, key):
        return self.as_singular().attr(key)

