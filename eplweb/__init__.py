from flask import Flask, render_template
import xml.etree.ElementTree as ET
from .contentMaker import ContentObject
from datetime import datetime
from werkzeug.routing import BaseConverter

app = Flask( 'epl-site' )

class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]

app.url_map.converters['regex'] = RegexConverter

@app.context_processor
def inject_now():
    return {'now': datetime.utcnow()}

@app.route('/')
def main():
    t = ET.parse('content.stubs/main.xml')
    oC = ContentObject( t.getroot() )
    return render_template( 'main.html', oC=oC )

@app.route('/cv/<regex("[a-zA-Z][a-zA-Z1-9_-]{1,63}"):uid>')
def cv( uid ):
    t = ET.parse('content.stubs/team/%s.xml'%uid)
    oC = ContentObject( t.getroot() )
    return render_template( 'cv.html', oC=oC )

